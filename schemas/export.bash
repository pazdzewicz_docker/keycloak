#!/bin/bash
PG_DUMP="$(which pg_dump)"

SCHEMA_FOLDER="/schemas/"
SCHEMA_FILE="export_$(date '+%Y-%m-%d_%H-%M-%S').psql"
SCHEMA_PATH="${SCHEMA_FOLDER}/${SCHEMA_FILE}"

${PG_DUMP} --host="localhost" --username="${POSTGRES_USER}" --dbname="${POSTGRES_DB}" > "${SCHEMA_PATH}"