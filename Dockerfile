ARG KEYCLOAK_VERSION="22.0.5"

FROM registry.access.redhat.com/ubi9/ubi-init:9.3 as init

RUN dnf -y install bash \
                   findutils && \
    dnf clean all

FROM quay.io/keycloak/keycloak:${KEYCLOAK_VERSION}

ENV KEYCLOAK_VERSION ${KEYCLOAK_VERSION}

COPY --from=init /usr/bin/find /usr/bin/find
COPY --from=init /usr/bin/which /usr/bin/which