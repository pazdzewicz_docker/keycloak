#!/bin/bash
KC_CLI="/opt/keycloak/bin/kc.sh"

SCHEMA_FOLDER="/realms"
SCHEMA_FILE="export_$(date '+%Y-%m-%d_%H-%M-%S')"
SCHEMA_PATH="${SCHEMA_FOLDER}/${SCHEMA_FILE}"

LATEST_SCHEMA_FILE="latest"
LATEST_SCHEMA_PATH="${SCHEMA_FOLDER}/${LATEST_SCHEMA_FILE}"

echo "EXPORT SCHEMA FILE ${SCHEMA_FILE}"
mkdir "${SCHEMA_PATH}"
${KC_CLI} export --dir "${SCHEMA_PATH}"
cp -r "${SCHEMA_PATH}" "${LATEST_SCHEMA_PATH}"
echo "DONE"