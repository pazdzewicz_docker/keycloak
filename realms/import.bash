#!/bin/bash
KC_CLI="/opt/keycloak/bin/kc.sh"
FIND="$(which find)"

REALM_FOLDER="/realms"
REALM_FILE="${1}"
REALM_PATH="${REALM_FOLDER}/${REALM_FILE}"

if [ -z ${REALM_FILE} ]
then
  echo "File does not exist"
  echo "Possible Folders:"
  ${FIND} "${REALM_FOLDER}" -type d -iname "export_*" -printf "- %f\n"
  exit 1
fi

echo "IMPORT REALM FILE ${REALM_FILE}"
${KC_CLI} import --dir "${REALM_PATH}"
echo "DONE"